import pytest
import sys

sys.path.append('./posts_man')
from api import api as posts_man_api

@pytest.fixture
def api():
    api = posts_man_api
    return api

@pytest.fixture
def client():
    api = posts_man_api
    client = api.test_client()
    yield client
