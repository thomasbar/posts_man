from random import randint
from tests import gen_data

def test_conn(client):
    assert client.get('/').status_code == 200

def test_conn_to_each_post(client):
    posts_quant = randint(5, 50)
    gen_data.many_inserts(posts_quant)
    check = 0
    for x in range (posts_quant):
        if client.get('/post/'+str(x)).status_code == 200:
            x = x + 1
    assert x == posts_quant

def test_allposts_conn(client):
    assert client.get('/posts').status_code == 200

def test_get_inserted_post(client):
    post = { 'uname': 'dobby', 'message': 'This is a test'}
    gen_data.data_insert(post)
    last_post = gen_data.len_col()
    response = client.get('/post/'+str(last_post)).json
    assert post['uname'] == response['uname'] and post['message'] == response['message']

#
# I don't understand this error. It seems that 'post' var is modified and gain an _id and a number as if it came from the mongodb...
#
#====================================================================== test session starts ========================================================================
#platform linux -- Python 3.7.3, pytest-5.2.2, py-1.8.0, pluggy-0.13.0
#rootdir: /home/barbot_tom/gitProject/unnamed
#plugins: flask-0.15.0
#collected 4 items                                                                                                                                                  
#posts_man/tests/test_conn_status.py ...F                                                                                                                     [100%]
#============================================================================ FAILURES =============================================================================
#_____________________________________________________________________ test_get_inserted_post ______________________________________________________________________
#client = <FlaskClient <Flask 'api'>>
#    def test_get_inserted_post(client):
#        post = { 'uname': 'dobby', 'message': 'This is a test'}
#        gen_data.data_insert(post)
#        last_post = gen_data.len_col()
#>       assert post == client.get('/post/'+str(last_post)).json
#E       AssertionError: assert {'_id': Objec...ame': 'dobby'} == {'message': '...ame': 'dobby'}
#E         Omitting 2 identical items, use -vv to show
#E         Left contains 2 more items:
#E         {'_id': ObjectId('5db98ab44daaf31cc616cde7'), 'number': 18}
#E         Use -v to get the full diff
#posts_man/tests/test_conn_status.py:23: AssertionError
#================================================================== 1 failed, 3 passed in 2.79s ====================================================================
#Makefile:2: recipe for target 'test' failed
#make: *** [test] Error 1

