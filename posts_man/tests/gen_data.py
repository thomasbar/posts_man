from random import randint, choice
from api import func
from api import col

user_list = ['Sofiane', 'Maxance', 'Beatrice', 'Deborah', 'Amandine', 'Coralie', 'Fabien', 'Alexandre', 'Stephane', 'Romain', 'Roberto']
lorem_text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam pellentesque magna et accumsan aliquet. Mauris elit eros, congue non risus vel, convallis aliquam dui. Pellentesque porttitor mauris metus, vitae convallis massa porta non. Fusce ac magna ante. Sed non nulla sed felis pretium fermentum vitae vitae augue. Cras aliquam neque consectetur, feugiat ex sed, lobortis magna. Suspendisse nibh erat, tincidunt in imperdiet nec, hendrerit sed sapien. Sed pellentesque posuere diam, id finibus mauris ornare at. Nulla facilisi. Curabitur id velit sit amet mauris rutrum venenatis. Maecenas venenatis vehicula ligula. Aliquam porta ex lorem, sit amet auctor enim dignissim sed. Nullam ante arcu, tempus non porttitor vitae, facilisis in felis. In faucibus leo felis, sed condimentum eros placerat ac. Aenean sodales vulputate leo. Sed sit amet malesuada eros. Donec sapien odio, condimentum non felis vitae, posuere euismod diam. Ut non orci sodales, ultricies ipsum pharetra, fermentum arcu. Phasellus ac ligula fringilla, ornare mi sed, eleifend odio. Sed eget aliquet orci. Morbi sed accumsan magna, pellentesque dictum enim. Phasellus in metus id turpis pellentesque blandit. Etiam semper diam vitae velit congue euismod. In euismod ligula lectus, sit amet ultrices orci mattis nec. Maecenas iaculis augue a neque mollis vulputate. Aliquam ac dapibus elit, a molestie tellus. In hac habitasse platea dictumst. Nam interdum blandit tellus, ut feugiat purus rhoncus ut. Nam vestibulum, sem quis semper fermentum, diam justo ullamcorper turpis, ut sagittis sapien lorem et odio. Nunc ipsum enim, posuere ut semper vitae, venenatis ac leo. Morbi ac eros non diam molestie hendrerit. Praesent faucibus enim non felis varius, a lacinia justo euismod.'

def comment(text):
    start = randint(0, len(lorem_text)-5)
    stop = randint(start, len(lorem_text))
    return text[start:stop]

def rand_insert():
    data = { 'uname': choice(user_list), 'message': comment(lorem_text)}
    func.add_num(data)
    col.insert_one(data)

def many_inserts(num):
    for a in range (num):
        rand_insert()

def len_col():
    return col.count_documents({})

def data_insert(data):
    func.add_num(data)
    col.insert_one(data)
