from flask import jsonify

from api import api

def json_added(number):
    message = { 'status': 200,
                'message': 'posts number '+str(number)+' correctly added to db'}
    resp = jsonify(message)
    resp.status_code = 200
    return resp

@api.errorhandler(415)
def not_json(error=None):
    message = { 'status': 415,
                'message': 'Unsupported media type. Only json accepted'}
    resp = jsonify(message)
    resp.status_code = 415
    return resp
