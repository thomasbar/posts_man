from flask import jsonify, request

from api import api
from api import status, func

POSTS_ARGS = ['max','uname']

@api.route('/add_post', methods = ['POST'])
def add_post():
    if request.headers['Content-Type'] == 'application/json':
        response = status.json_added(func.add_data(request.json))
    else:
        response = status.not_json()
    return response

@api.route('/posts', methods = ['GET'])
def posts():
    if not func.args_check(POSTS_ARGS, request.args):
        data = { 'message': 'Unsupported argument(s)' } # Is there a status code for this ?
    else:
        data = func.all_data(**request.args)
    response = jsonify(data)
    return response

@api.route('/post/<int:number>', methods = ['GET'])
def get_a_post(number):
    data = func.get_one(number)
    return jsonify(data)

@api.route('/', methods = ['GET'])
def hello():
    return 'hello you :)'
