from flask import Flask
from pymongo import MongoClient
from os import environ

api = Flask(__name__)
try:
#    sel_keys = [x for x in environ.keys() if 'mongodb' in x]
#    mongo_vars = { key: environ[key] for key in sel_keys }
#    print(mongo_vars)
#    client = MongoClient(**mongo_vars)
    client = MongoClient(host=environ['mongodb_host'],
                         port=int(environ['mongodb_port']),
                         username=environ['mongodb_uname'],
                         password=environ['mongodb_psswrd'],
                         authSource="admin")
    col = client.posts_db.posts_collection
except:
    try:
        client = MongoClient(host=environ['mongodb_host'],
                             port=int(environ['mongodb_port']))
        col = client.posts_db.posts_collection
    except:
        print('Unable to connect to database at '+':'.join([environ['mongodb_host'],environ['mongodb_port']]))

from api import routes
from api import status
