from api import col

def add_num(posts):
    post_num = col.count_documents({}) + 1
    posts['number'] = post_num
    return posts

def all_data(**kwargs):
    data = get_all()
    if kwargs:
        if 'uname' in kwargs.keys():
            data = [get_all()[x] for x in len(get_all())\
                    if get_all()[x]['uname'] == kwargs['uname']]
        if 'max' in kwargs.keys():
            data = data[:kwargs['max']]
    return data

def add_data(document):
    num_data = add_num(document)
    col.insert_one(num_data)
    return num_data['number']

def get_all():
    data = [col.find_one({ 'number': x}, {'_id': 0, 'number': 0 }) for x in range (col.count_documents({}), 0, -1)]
    return data

def get_one(number):
    data = col.find_one({ 'number': number }, {'_id': 0, 'number': 0 })
    return data

def args_check(arg_list, *args):
    response = True
    if len(args) > 1:
        for x in args[1:]:
            if x not in arg_list:
                response = False
    return response
