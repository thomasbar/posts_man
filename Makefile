test:
	sudo docker run -d -p 27018:27017 --env-file=test_db_envvar --name=test_db mongo
	export mongodb_uname=dobby;\
	export mongodb_psswrd=deatheater;\
	export mongodb_host=0.0.0.0;\
	export mongodb_port=27018;\
	pipenv run pytest
	sudo docker kill test_db
	sudo docker rm test_db

docker_sweep:
	sudo docker kill test_db
	sudo docker rm test_db
